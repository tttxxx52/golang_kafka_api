package handler

import (
	"api/content"
	"api/util"
	"api/util/kafka"
	"api/util/log"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"os/exec"
)

type TestHandler content.Handler

func (handler *TestHandler) TestKafkaApi(c *gin.Context) interface{} {
	type ReceivedData struct {
		Id int64 `json:"id"`
	}
	var data ReceivedData
	if err := json.Unmarshal([]byte(c.GetString("parameters")), &data); err != nil {
		return util.RS{Message: "", Status: false}
	}
	count := 0
	//產生uuid
	out, _ := exec.Command("uuidgen").Output()
	uuid := string(out)
	//建立要傳送的資料(記得傳入uuid
	jsonMap := map[string]interface{}{
		"uuid":   uuid,
		"userId": c.GetInt("userId"),
		"action": "Test", // have router
	}

	_, err := kafka.Push("Request", jsonMap)
	log.Error(err)
	dataList := make(map[string]interface{}, 0)
	kafka.Listen("Response", func(value []byte) (isBreak bool) {
		var kafKaRS util.KafKaRS
		if err := json.Unmarshal(value, &kafKaRS); err != nil {
			log.Error(err)
			dataList["status"] = false
			dataList["message"] = "error"
			return true
		} else if uuid == kafKaRS.Uuid {
			dataList["status"] = kafKaRS.Status
			dataList["message"] = kafKaRS.Message
			return true
		} else if count > 10 {
			dataList["status"] = false
			dataList["message"] = "time out"
			return true
		}
		count++
		return false
	})
	return dataList
}
