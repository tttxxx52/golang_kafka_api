package fcm

import (
	"api/model"
	"api/util/log"
	"github.com/NaySoftware/go-fcm"
)

type FCM struct {
	appId 		 int64
	userId 		 int64
	fcmServerKey string
	token 		 []string
	title 		 string
	body 		 string
	data 		 map[string]interface{}
}

func (f *FCM) SetAppId(appId int64) *FCM {
	f.appId = appId
	return f
}

func (f *FCM) SetUserId(userId int64) *FCM {
	f.userId = userId
	return f
}

func (f *FCM) SetFcmServerKey(fcmServerKey string) *FCM {
	f.fcmServerKey = fcmServerKey
	return f
}

func (f *FCM) SetTokens(token []string) *FCM {
	f.token = token
	return f
}

func (f *FCM) SetTitle(title string) *FCM {
	f.title = title
	return f
}

func (f *FCM) SetBody(body string) *FCM {
	f.body = body
	return f
}

func (f *FCM) SetData(data map[string]interface{}) *FCM {
	f.data = data
	return f
}

func (f *FCM) Send() {
	var (
		deviceToken model.DeviceToken
		np 			fcm.NotificationPayload
	)

	tokens := deviceToken.SetAppId(f.appId).SetUserId(f.userId).QueryToken()

	c := fcm.NewFcmClient(f.fcmServerKey)
	c.NewFcmRegIdsMsg(tokens, f.data)

	np.Title = f.title
	np.Body = f.body
	c.SetNotificationPayload(&np)
	status, err := c.Send()

	if err == nil {
		status.PrintResults()
	} else {
		log.Error(err)
	}

}
