package config

import (
	"github.com/gin-gonic/gin"
	"os"
)

type configServer struct {
	Host string
}

var ServerConfig configServer

func initServerConfig() {

	switch gin.Mode() {
	case gin.ReleaseMode:
		ServerConfig = configServer{"http://test/"}
	case gin.DebugMode:
		ServerConfig = configServer{os.Getenv("HOST")}
	case gin.TestMode:
		ServerConfig = configServer{"http://test/"}
	}
}