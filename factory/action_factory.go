package factory

import (
	"api/handler"
)

var (
	testHandler         handler.TestHandler
)

var ActionFactoryAuth = map[string]interface{}{
}

var ActionFactory = map[string]interface{}{
	//======================================================================
	//						Not Auth Handler Api
	//======================================================================
	"TestKafkaApi":              &testHandler,
}
